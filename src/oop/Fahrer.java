/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *verwaltet die Aktionen des Autofahrers
 * @author admin
 */
public class Fahrer {
	/**
	 * zahlZeit des Fahrers
	 */
    private int zahlZeit;
    /**
     * Anstehzeit des Fahrers
     */
    private int anstehZeit;
    /**
     * verhindert zB dass geprueft werden muss ob der Fahrer in der Queue der Kasse ist.
     * (der Rechenaufwand sinkt damit erheblich, insbesondere bei langen Kassenschlangen)
     */
    private int state;
    //0:=fahrer im oder am auto
    //1:=fahrer zahlt 
    //2:=fahrer raeumt
    /**
     * erstellt einen neuen Fahrer
     * @param zahlZeit 
     */
    public Fahrer(int zahlZeit) {
        this.zahlZeit = zahlZeit;
        anstehZeit=0;
        state=0;
    }
    
    /**
     * simulier das Andauern des Zahlvorgangs an der Kasse
     * @return verbleibende Zahlzeit
     */
    public int zahlen(){
        if (zahlZeit>0){
            if (state==0){state+=1;}
            zahlZeit-=1;
        }
        return zahlZeit;
    }
    
    /**
     * der Fahrer wird zur Kasse geschickt, bezahlt dort, bzw. stellt sich ggf zuerst in der Schlange an
     * @param kasse Kasse an die der Fahrer geschickt wird
     */
    public void goToKasse(Kasse kasse){
        if (state==0){
            kasse.addFahrer(this);
            state=1;
        }
    }
    
    /**
     * laesst eine Anstehzeiteinheit verstreichen und vermerkt sie in der Anstehzeit des Fahrers
     * @return
     */
    public int anstehen(){
    	anstehZeit+=1;
    	return anstehZeit;
    }

    /**
     * gibt die Anstehzeit des Fahrers zurueck
     * @return Anstehzeit
     */
    public int getAnstehZeit() {
	return anstehZeit;
    }
    
    /**
     * gibt die benoetigte Zahlzeit an der Kasse zurueck
     * @return Zahlzeit
     */
    public int getZahlZeit() {
        return zahlZeit;
    }
}
