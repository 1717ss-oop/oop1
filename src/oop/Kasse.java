/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

import java.util.*;
/**
 * verwaltet die Tankstellenkasse und deren Kunden
 * @author Greta
 *
 */
public class Kasse {
	/**
	 * @param fahrerschlange speichert die anstehenden Fahrer der vollgetankten Autos 
	 * @param aktFahrer der Fahrer der gerade bezahlt
	 */
	private Queue <Fahrer> fahrerschlange;
	private Fahrer aktFahrer;
	
	/**
	 * erstellt eine neue Kasse
	 */
	public Kasse(){
		fahrerschlange=new LinkedList<Fahrer>();
	}
	
	/**
	 * stellt die Moeglichkeiten und Aktionen dar, die in einer Sekunder an der Kasse passieren koennen
	 * @return die Anzahl der anstehenden Fahrer
	 */
	public int tack(){
		try{
	            if (aktFahrer ==null) aktFahrer = fahrerschlange.remove();
	        }catch (Exception e){					// wenn keine Bezahlt und auch keiner ansteht
	            return 0;
	        }
	        if (aktFahrer.getZahlZeit()<=0){
	            if (!fahrerschlange.isEmpty()){
                        aktFahrer = fahrerschlange.remove();
                    }
	            else
                    {   
                        aktFahrer = null;
                        return 0;
                    }
	        }else{
                    aktFahrer.zahlen();
                }

	        for (Fahrer fahrer:fahrerschlange){
	        	fahrer.anstehen();
	        }
	        return fahrerschlange.size();
	}

    
	/**
	 * fuegt einen neuen Fahrer in die fahrerschlange ein
	 * @param f stellt den Fahrer dar, der sich hinten anstellt
	 */
	public Boolean addFahrer(Fahrer f){ 
            if (fahrerschlange.contains(f)){
                return false;
            }else{
                fahrerschlange.add(f);
                return true;
            }
	}
     /**
	 * gibt die anstehschlange vor der kasse zurueck
	 * @return fahrerschlange
	 */
	public Queue<Fahrer> getFahrerschlange(){
		return fahrerschlange;
	}
}
