/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

import java.util.ArrayList;

/**
 * Verwaltet die Simulation und deren Auswertung
 * @author Greta
 *
 */
public class Tankstelle {
	/**
	 * @param zapfsaeulen die Zapfsaeulen der Tankstelle
	 * @param kasse die Kasse der Tankstelle
	 * @param simulationszeit die Gesamtsimulationszeit
	 * @param protokollzeit Zeitabstaende in denen gemessen und ausgegeben wird
	 * @param wuerfel zugehoehriges Wuerfelobjekt, welches die zufallszeiten fuer neu ankommende autos berechnet
	 * @param fertigeAutos eine Liste aller fertig getankten und bezahlten Autos, zur Auswertung
	 */
	private ArrayList<ZapfS> zapfsaeulen;
	private Kasse kasse;
	private int simulationszeit;
	private int protokollzeit;
	private Wuerfel wuerfel;
	private ArrayList<Auto> fertigeAutos = new ArrayList<Auto>();
	
	/**
	 * erstellt eine neue Tankstelle
	 * @param sim uebergibt die Simulationszeit
	 * @param prot bestimmt in welchen zeitabstaenden protokolliert werden soll
	 */
	public Tankstelle(int sim, int prot){
		zapfsaeulen = new ArrayList<ZapfS>();
		kasse = new Kasse();
		this.simulationszeit=sim;
		this.protokollzeit=prot;
	}
	
	/**
	 * gibt die, zur Tankstelle gehoerende Kasse zurueck
	 * @return Kasse
	 */
    public Kasse getKasse(){
            return kasse;
        }
        /**
         * sammle fertigeAutos fuerr Auswertung
         * @param auto wird hinzugefuegt
         * @return sollte immer true sein
         */
    public Boolean addFertigesAuto(Auto auto){
            if (fertigeAutos.contains(auto)){
                return false;//sollte nie passieren
            }else{
                fertigeAutos.add(auto);
                return true;
            }
        }
    /**
     * simuliert einen durchlauf der simulationszeit in sec, 
     * fuehrt dabei ggf. folgende Funktionen aus: 
     * -anstellen eines neuen Autos
     * -Ausgabe der Protokollwerte
     * -normale Warte- und Tankeinheit, in der sich die Fahrer ggf an die Kasse bewegen
     */
	public void simulation(){
		kopf();
		int p=protokollzeit+1;
		int autokommt=wuerfel.randomAuto();
		
		for (int i=0; i<=simulationszeit;i++){
			autokommt--; 
			if (autokommt==0){ 	// neues Auto stellt sich an
				autoAnstellen();
				autokommt=wuerfel.randomAuto();
			}
			
			p--;
			if(i==0||p==0){		//Ausgabe der Protokollwerte
				ausgabeP(i);
				p=protokollzeit;
			}
				
            //normale Sec vergeht
            kasse.tack();
            for(ZapfS s:zapfsaeulen){
                s.tick();
            }
		}
	}
	/**
	 * wertet die Liste der fertigen Autos, auf durchschnittliche und hoechste Wartezeiten aus
	 */
	public void auswertung(){
		int wartenZapf=0;
		int maxwartenZapf=0;
		int wartenKasse=0;
		int maxwartenKasse=0;
		System.out.println();
		
		for(Auto auto: fertigeAutos){
			wartenZapf+=auto.getWarteZeit();
			wartenKasse+=auto.getFahrer().getAnstehZeit();
			if (maxwartenZapf<auto.getWarteZeit()){
				maxwartenZapf=auto.getWarteZeit();
			}
			if (maxwartenKasse<auto.getFahrer().getAnstehZeit()){
				maxwartenKasse=auto.getFahrer().getAnstehZeit();
			}
		}
		try{
		wartenZapf=wartenZapf/fertigeAutos.size();
		wartenKasse=wartenKasse/fertigeAutos.size();
		}catch (ArithmeticException e){
			System.out.println("\nEs war niemand tanken.");
		}
		if (!fertigeAutos.isEmpty()){
			
		System.out.println("Durchschnittliche Wartezeiten:\n"
				+ "an den Zapfsaeulen:	"+ ((double) Math.round(((double) wartenZapf/60) * 100) / 100) + "min ("+ wartenZapf+"sec)\n"
				+ "an der Kasse:		"+ ((double) Math.round(((double) wartenKasse/60) * 100) / 100) + "min ("+ wartenKasse+"sec)\n");
		System.out.println("Maximale Wartezeiten:\n"
				+ "an den Zapfsaeulen:	"+ ((double) Math.round(((double) maxwartenZapf/60) * 100) / 100) + "min ("+ maxwartenZapf+"sec)\n"
				+ "an der Kasse:		"+ ((double) Math.round(((double) maxwartenKasse/60) * 100) / 100) + "min ("+ maxwartenKasse+"sec)\n");
		}
		
		System.out.println("SIMULATIONS-ENDE\n Zum Beenden des Programms *Enter* druecken.");
		Testprogramm.scan.nextLine();
		System.exit(0);
	}
	/**
	 * fuegt der Tankstelle die wuerfelklasse, zum berechnen aller zufallszahlen
	 * @param wuerfel
	 */
	public void setWuerfel(Wuerfel wuerfel) {
		this.wuerfel = wuerfel;
	}
	/**
	 * fuegt eine neue Zapfsaeule zur Tankstelle hinzu
	 */
	public void addZapfS(){
		zapfsaeulen.add(new ZapfS(this));
	}
    /**
     * gibt die Liste zur Auswertung zurueck  
     * @return Liste fertiger Autos
     */
	public ArrayList<Auto> getFertigeAutos() {
		return fertigeAutos;
	}
	
	/**
	 * gibt den Tabellenkopf aus
	 */
	private void kopf(){
		System.out.println("Mit *Enter* starten Sie die Simulation.");
		Testprogramm.scan.nextLine();
		System.out.print("WARTESCHLANGEN\n___ZEIT___");
		for (int i=0;i<zapfsaeulen.size();i++){
			System.out.print("___SAEULE "+ (i+1)+"__");
		}
		System.out.println("___KASSE___");
	}
	
	/**
	 * ermittelt eine zufaellige Zapfsaeule an die sich ein neues Auto anstellt
	 */
	private void autoAnstellen(){
		int saeule=Wuerfel.randomSaeule(zapfsaeulen.size());
		Auto auto= new Auto(wuerfel.randomTankzeit(),wuerfel.randomRaeumzeit(),wuerfel.randomZahlzeit());
		zapfsaeulen.get(saeule).addAuto(auto);
	}
    
	/**
	 * gibt eine neue Zeile der Tabelle zurueck
	 * @param i	gibt die aktuelle Zeiteinheit zurueck (hier in sec)
	 */
	private void ausgabeP(int i){
		i=i/60;
		if(i<10)				{System.out.print("    00"+(i)+"    ");}
		else if(i>9 && i<100)	{System.out.print("    0"+(i)+"    ");}
		else					{System.out.print("    "+(i)+"    ");}
		for (int j=0;j<zapfsaeulen.size();j++){
			System.out.print("      "+zapfsaeulen.get(j).tick()+"      ");

		}
		System.out.println("     "+kasse.tack());
	}
    /*der Fahrer stellt sich jetzt freiwillig an.
	private void fahrerAnstellen(){
		for(ZapfS s:zapfsaeulen){
			Auto aktAuto=s.getAktAuto();
			if(aktAuto!=null){
				if (aktAuto.getTankzeit()<=0 && (!kasse.getFahrerschlange().contains(aktAuto.getFahrer()))){
					kasse.addFahrer(s.getAktAuto().getFahrer());
				}
			}
		}
	} 
        */
}
