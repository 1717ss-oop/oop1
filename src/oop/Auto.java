/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *
 * @author admin
 */
public class Auto {
    private Fahrer fahrer;
    private int tankZeit;
    private int raeumZeit;
    private int warteZeit;
    
    /**
     * erstellt ein neues Auto und dessen Fahrer
     * @param tankZeit zufaellige Zeit, die das Auto zum tanken braucht
     * @param raeumZeit zufaellige Zeit, die das Auto zum raeumen braucht
     * @param zahlZeit zufaellige Zeit, die der Fahrer zum bezahlen braucht
     */
    public Auto(int tankZeit, int raeumZeit,int zahlZeit) {
        this.tankZeit = tankZeit;
        this.raeumZeit = raeumZeit;
        this.fahrer=new Fahrer(zahlZeit);
    }    
    /**
     * zaehlt die Warte Zeit hoch
     * @return bisher gewartete Zeit
     */
    public int warten(){
        warteZeit+=1;
        return warteZeit;
    }
    
    /**
     * zaehlt die Zeiten um eine Zeiteinheit runter und simuliert somit eine vergangene Zeiteinheit
     * @param kasse
     * @return restliche Gesamtzeit, die das Auto noch Aktionen durchfuehrt(tanken,raeumen) brauch
     */
    public int tanken(Kasse kasse){
        if (tankZeit>0){
            tankZeit-=1;
            //return tankZeit+raeumZeit;
        }else if (fahrer.getZahlZeit()>0){
            fahrer.goToKasse(kasse);
            //return raeumZeit;
        }else if (raeumZeit > 0){
            raeumZeit-=1;
            //return raeumZeit;
        }
        return tankZeit+raeumZeit;
    }
	
    /**
     * gibt die aktuelle/gesamte Wartezeit vor der Saeule zurueck
     * @return Wartezeit des Autos
     */
    public int getWarteZeit() {
		return warteZeit;
    }
    /**
     * gibt das Fahrerobjekt des Autos zurueck
     * @return AutoFahrer
     */
    public Fahrer getFahrer() {
            return fahrer;
    }
	
}