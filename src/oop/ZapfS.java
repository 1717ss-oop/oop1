/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

import java.util.*;
/**
 * erstellt eine neue Zapfsaeule
 */
public class ZapfS {
	/**
	 * anstehende Autos vor der Saeule
	 */
    private Queue<Auto> autos;
    /**
     * aktuelles Auto, dass gerade tankt
     */
    private Auto aktAuto;
    /**
     * Tankstelle zu der die Saeule gehoert
     */
    private Tankstelle tankstelle;
    
    /** 
     * erstellt eine neue Zapfsaeule
     * @param tankstelle zu der die Saeule gehoert
     */
    public ZapfS(Tankstelle tankstelle) {
        autos = new LinkedList<Auto>();
        this.tankstelle=tankstelle;
    }
/**
 *  laesst eine Zeiteinheit (sec) an der Saeule vergehen,
 *  dh.das akt. Auto faehrt vielleicht weg, bzw. ein neues Auto stellt sich an
 * @return
 */
    public int tick(){
        try{
            if (aktAuto==null) aktAuto = autos.remove();
        }catch (Exception e){
            return 0;//Es tankt kein Auto und es steht kein Auto an
        }
        int restzeit=aktAuto.tanken(tankstelle.getKasse());
        /* nicht mehr notwendig
        if (aktAuto.getTankzeit()==0 && aktAuto.getFahrer().getState()==0){
            tankstelle.getKasse().addFahrer(aktAuto.getFahrer());
        */
        if (restzeit<=0){ 
            tankstelle.addFertigesAuto(aktAuto);
            if (!autos.isEmpty()){
                aktAuto = autos.remove();
            } else{
                aktAuto = null;
                return 0; //Es tankt kein Auto und es steht kein Auto an
            }
        }//else das Auto/der Fahrer ist noch nicht fertig.--> nothing to do
        for (Auto auto : autos){
            auto.warten();
        }
        return autos.size(); //Laenge der Schlange ohne tankendes Auto
    }
    /**
     * fuegt ein neues Auto in die Anstehschlange ein
     * @param auto
     * @return laenge der warteschlange
     */
    public int addAuto(Auto auto){
        autos.add(auto);
        return autos.size();
    }
    /**
     * gibt das tankende Auto zurueck
     * @return aktuelles Auto
     */
    public Auto getAktAuto() {
        return aktAuto;
    }

}
