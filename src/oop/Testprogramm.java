package oop;

import java.util.Scanner;
/**
 * 
 * @author Greta
 *
 */
public class Testprogramm {
	
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Tankstelle tankstelle = eingabeT();
		Wuerfel wuerfel= eingabeW();
		tankstelle.setWuerfel(wuerfel);
		
		tankstelle.simulation();
		tankstelle.auswertung();

	}
	/**
	 * Interaktion mit Nutzer, der die Simulationszeit usw. eingibt. 
	 * @return neu erstellte Tankstelle
	 */
	public static Tankstelle eingabeT(){
                int zapfs = -1;
                while (zapfs<1){
                    try{
                        System.out.println("TANKSTELLEN-SIMULATION\nZahl der Zapfsaeulen?");
                        zapfs=Integer.decode(scan.next());
                    }catch(Exception e){
                        
                    }
                }
                int simulationszeit = -1;
                while (simulationszeit<1){
                    try{
                        System.out.println("Simulationszeit? (min)");
                        simulationszeit = Integer.decode(scan.next())*60;
                    }catch(Exception e){
                        
                    }
                }
               int protokollzeit  = -1;
                while (protokollzeit<1){
                    try{
                        System.out.println("Protokollabstaende? (min)");
                        protokollzeit = Integer.decode(scan.next())*60;
                    }catch(Exception e){
                        
                    }
                }


                
		Tankstelle tankstelle= new Tankstelle(simulationszeit, protokollzeit);
		for (int i=0; i<zapfs;i++){
			tankstelle.addZapfS();
		}
		return tankstelle;
	}
	
	/**
	 * Interaktion mit Nutzer, dieser kann die default Simulationszeiten akzeptieren oder neue festlegen
	 * @return neu erstelltes Wuerfelobjekt
	 */
	public static Wuerfel eingabeW(){
                int deltat  = -1;
                while (deltat<1){
                    try{
                        System.out.println("Durchschnittlicher Zeitabstand ankommender Autos? (min)");
                        deltat = Integer.decode(scan.next());
                    }catch(Exception e){
                        
                    }
                }
		Wuerfel wuerfel = new Wuerfel(deltat);
		while (true){
                    try{
                       System.out.println("Voreingestelle Verweilzeiten: \n"
				+ "Tanken: 200s bis 300s\n"
				+ "Bezahlen: 200s bis 500s\n"
				+ "Raeumen der Tankstelle: 30 bis 70s\n"
				+ "\nWollen Sie diese Werte beibehalten? (*y* zum bestaetigen, etwas anderes um Werte zu aendern)");
                       if (scan.next().equals("y")){
                           return wuerfel;
                       }else{
                           break;
                       }
                    }catch(Exception e){
                        //shouldnt happen
                    }
                }
		 //neue Einstellungen
			int tank1=1; 
			int tank2=0;
			int zahl1=1;
			int zahl2=0;
			int raeum1=1;
			int raeum2=0;
			
			System.out.println("Neue Verweilzeiten:");
			while (tank1>=tank2 | tank1<1 | tank2 < 1){
                            try{
				System.out.println("Minimaltankzeit? (in sec)");
				tank1= Integer.decode(scan.next());
				System.out.println("Maximaltankzeit? (in sec)");
				tank2= Integer.decode(scan.next());
			
                            }catch(Exception e){
                        
                            }
                        }
			while(zahl1>=zahl2 | zahl1<1 | zahl2 < 1){
                            try{
                                System.out.println("Minimalzahlzeit? (in sec)");
				zahl1= Integer.decode(scan.next());
				System.out.println("Maximalzahlzeit? (in sec)");
				zahl2= Integer.decode(scan.next());
                            }catch(Exception e){
                        
                            }
                        }
			while(raeum1>=raeum2 | raeum1<1 | raeum2 < 1){
                            try
                                {System.out.println("Minimalraeumzeit? (in sec)");
                                raeum1= Integer.decode(scan.next());
                                System.out.println("Maximalraeumzeit? (in sec)");
                                raeum2= Integer.decode(scan.next());

                            }catch(Exception e){

                            }
                        }
			
			System.out.println("Die neuen Verweilzeiten wurden gespeichert.");
			wuerfel.setTanken(tank1,tank2);
			wuerfel.setZahlen(zahl1, zahl2);
			wuerfel.setRaeumen(raeum1, raeum2);
			
			scan.nextLine();
			return wuerfel;
        }
	}

