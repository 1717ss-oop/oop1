package oop;

/**
 * "erwuerfelt" Zufallszahlen fuer die Simulationszeiten
 * @author Greta
 *
 */
public class Wuerfel {
	/**
	 * @param autosankommen durchschnittliche Ankunftszeit neue Autos
	 * restliche parameter  default einstellungen der Ablaufzeiten
	 */
	int autosankommen; //in min angegeben
	int tank1=200;
	int tank2=300;
	int zahl1=200;
	int zahl2=500;
	int raeum1=30;
	int raeum2=70;
	
	/**
	 * erstellt eine neues Wuerfelobjekt, mit default einstellungen
	 * @param autosankommen
	 */
	public  Wuerfel(int autosankommen){
		this.autosankommen=autosankommen;
	}
	
	/**
	 * aendert die Tankzeiten
	 * @param tank1 minimal
	 * @param tank2 maximal
	 */
	public void setTanken(int tank1, int tank2){
		this.tank1=tank1;
		this.tank2=tank2;
	}
	/**
	 * aendert die Zahlzeiten
	 * @param zahl1 minimal
	 * @param zahl2 maximal
	 */
	public void setZahlen(int zahl1, int zahl2){
		this.zahl1=zahl1;
		this.zahl2=zahl2;
	}
	/**
	 * aendert die raeumzeiten
	 * @param raeum1 minimal
	 * @param raeum2 maximal
	 */
	public void setRaeumen(int raeum1, int raeum2){
		this.raeum1=raeum1;
		this.raeum2=raeum2;
	}
	
	/**
	 * errechnet eine zufaellige Tankzeit 
	 * @return zeit 
	 */
	public int randomTankzeit(){
		return ((int)(Math.random() * (tank2-tank1)) + tank1);
	}
	/**
	 * errechnet eine zufaellige Zahlzeit
	 * @return zeit
	 */
	public int randomZahlzeit(){
		return ((int)(Math.random() * (zahl2-zahl1)) + zahl1);
	}	
	/**
	 * errechnet eine zufaellige Raeumzeit
	 * @return zeit
	 */
	public int randomRaeumzeit(){
		return ((int)(Math.random() * (raeum2-raeum1)) + raeum1);
	}
	/**
	 * errechnet eine zufaellige Ankunftszeit, auf basis der durchschnittlichen Ankunftszeit
	 * @return zeit
	 */
	public int randomAuto(){
		return ((int)(Math.random() * (((autosankommen*2)-1)*60)) + 1);

	}
	
	/**
	 * waehlt eine zufaellige Tanksaeule aus
	 * @param anz anzahl der Saeulen
	 * @return ausgewaehlte Saeule
	 */
	public static int randomSaeule(int anz){
		return ((int)(Math.random() * (anz)));
		
	}
}
